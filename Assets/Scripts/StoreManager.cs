﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoreManager : MonoBehaviour
{
    [Header("Remove Ads")]
    public GameObject adsRemove, adsRemoveFailed, adsButton, restoreButton;

    [Header("Unlock Learning Mode")]
    public GameObject weatherButton, clothesButton, objectButton, colorsButton, iapPanel;

    private void Update()
    {
        if(weatherButton == null || clothesButton == null || objectButton == null || colorsButton == null || iapPanel == null)
        {
            return;
        }
        else
        {
            if (PlayerPrefs.GetInt("unlock") != 1)
            {
                weatherButton.SetActive(true);
                clothesButton.SetActive(true);
                objectButton.SetActive(true);
                colorsButton.SetActive(true);
            }
            else
            {
                weatherButton.SetActive(false);
                clothesButton.SetActive(false);
                objectButton.SetActive(false);
                colorsButton.SetActive(false);
            }
        }
        
    }

    public void OnRemoveAdsComplete(){

        PlayerPrefs.SetInt("removeAds", 1);
        adsButton.GetComponent<Button>().interactable = false;

        //restoreButton.SetActive(true);
        //adsRemove.SetActive(true);
        //StartCoroutine(AdsRemoved());
    }

     public void OnRemoveAdsFailed(){

        PlayerPrefs.SetInt("removeAds", 0);
        adsButton.GetComponent<Button>().interactable = true;

        //restoreButton.SetActive(false);
        //adsRemoveFailed.SetActive(true);
        //StartCoroutine(AdsRemovedFailed());
    }

    public void OnUnlockLearningComplete(){

        PlayerPrefs.SetInt("unlock", 1);
        iapPanel.SetActive(false);
    }

    public void OnUnlockLearningFailed()
    {
        PlayerPrefs.SetInt("unlock", 0);
        iapPanel.SetActive(true);
    }

    IEnumerator AdsRemoved()
    {
        yield return new WaitForSeconds(3);
        adsRemove.SetActive(false);
    }
    IEnumerator AdsRemovedFailed()
    {
        yield return new WaitForSeconds(3);
        adsRemoveFailed.SetActive(false);
    }

}
