﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharadesCategoryScript : MonoBehaviour
{
    public void BackButton()
    {

    }
    public void SelectAnimal()
    {
        string game = PlayerPrefs.GetString("game");

        if(game == "charades")
        {
            SceneManager.LoadScene("CH_AN_Gameplay");
        }
        else if (game == "puzzle")
        {
            SceneManager.LoadScene("MainPuzzle");
        }
        else
        {
            SceneManager.LoadScene("MatchPicture");
        }
    }
}
