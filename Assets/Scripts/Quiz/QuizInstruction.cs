﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class QuizInstruction : MonoBehaviour
{
    public void BackButton()
    {
        SceneManager.LoadScene("Menu");
    }
    public void StartQuiz()
    {
        SceneManager.LoadScene("QU_Gameplay");
    }
}
