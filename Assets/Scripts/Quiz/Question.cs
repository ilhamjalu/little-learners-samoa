﻿using UnityEngine;
[System.Serializable]
public class Question
{
    public Sprite image;
    public string englishName;
    public Answer[] samoanAnswers = new Answer[4];
}
