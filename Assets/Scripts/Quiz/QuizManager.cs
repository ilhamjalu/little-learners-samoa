﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class QuizManager : MonoBehaviour
{
    public Question[] questions;
    private int gameScore = 0;
    private int life = 3;
    public bool isWin;

    private static List<Question> unansweredQuestions;
    private Question currentQuestion;

    [SerializeField] private Image spriteImage;
    [SerializeField] private TextMeshProUGUI englishText;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private float timeBetweenQuestions = 1f;
    [SerializeField] private TextMeshProUGUI[] samoanText;
    [SerializeField] private Image[] lifeImages;
    [SerializeField] private GameObject correctPanel, wrongPanel, gameOverPanel, winPanel, optionPanel, backPanel;



    void Start()
    {
        if (unansweredQuestions == null || unansweredQuestions.Count == 0)
        {
            unansweredQuestions = questions.ToList<Question>();
        }
        SetCurrentQuestion();
    }

    void SetCurrentQuestion()
    {
        int randomQuestionIndex = Random.Range(0, unansweredQuestions.Count);
        currentQuestion = unansweredQuestions[randomQuestionIndex];

        spriteImage.sprite = currentQuestion.image;
        englishText.text = currentQuestion.englishName;
       
        for (int i = 0; i < 4; i++)
        {
            samoanText[i].text = currentQuestion.samoanAnswers[i].answer;
        }


    }
    public void UserSelectTrue(int i)
    {
        if (currentQuestion.samoanAnswers[i].isTrue)
        {
            if(gameScore < questions.Length - 1){
                CorrectClick();
            }
            gameScore += 1;
            scoreText.text = gameScore.ToString();
            correctPanel.SetActive(true);

            if (gameScore == questions.Length  && !isWin)
            {
                isWin = true;
                winPanel.SetActive(true);
                WinClick();
            }

        }
        else
        {
            isWin = false;
            if(life > 1){
                WrongClick();
            }
            lifeImages[life - 1].gameObject.SetActive(false);
            life -= 1;
            wrongPanel.SetActive(true);

            if (life == 0)
            {
                gameOverPanel.SetActive(true);
                GameOverClick();
            }
        }

        StartCoroutine(TransitionToNextQuestion());
    }
    IEnumerator TransitionToNextQuestion()
    {
        unansweredQuestions.Remove(currentQuestion);
        yield return new WaitForSeconds(timeBetweenQuestions);
        correctPanel.SetActive(false);
        wrongPanel.SetActive(false);
        Start();
    }


    public void ButtonClickA()
    {
        UserSelectTrue(0);
    }
    public void ButtonClickB()
    {
        UserSelectTrue(1);
    }
    public void ButtonClickC()
    {
        UserSelectTrue(2);
    }
    public void ButtonClickD()
    {
        UserSelectTrue(3);
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("QU_Instruction");
    }

    public void BackButton()
    {
        SceneManager.LoadScene("QU_Instruction");
    }


    public void ActivePanel()
    {
        if (optionPanel.activeInHierarchy || backPanel.activeInHierarchy)
        {
            //countDown = false;
        }
        else
        {
            //countDown = true;
        }
    }

    public void BGMClick()
    {
        if (Menu.bgmOn)
        {
            Menu.bgmOn = false;
            FindObjectOfType<AudioManager>().Mute("Theme");
        }
        else
        {
            Menu.bgmOn = true;
            FindObjectOfType<AudioManager>().UnMute("Theme");
        }
    }
    public void SFXClick()
    {
        if (Menu.sfxOn)
        {
            Menu.sfxOn = false;
        }
        else
        {
            Menu.sfxOn = true;
        }
    }

    public void ButtonClick()
    {
        if(Menu.sfxOn)
        {
            FindObjectOfType<AudioManager>().Play("Click");
        }
    }
    public void CorrectClick()
    {
        if(Menu.sfxOn)
        {
            FindObjectOfType<AudioManager>().Play("Correct2");
        }
    }
    public void WrongClick()
    {
        if(Menu.sfxOn)
        {
            FindObjectOfType<AudioManager>().Play("Wrong");
        }
    }
    public void GameOverClick()
    {
        if(Menu.sfxOn)
        {
            FindObjectOfType<AudioManager>().Play("GameOver");
        }
    }
    public void WinClick()
    {
        if(Menu.sfxOn)
        {
            FindObjectOfType<AudioManager>().Play("Win");
        }
    }

}
