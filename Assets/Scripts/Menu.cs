﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public static bool sfxOn;
    public static bool bgmOn;


    void Start()
    {
        sfxOn = true;
        bgmOn = true;
      
        if (Menu.bgmOn)
        {
            FindObjectOfType<AudioManager>().Play("Theme");
        }
    }
    public void PlayGame()
    {
        SceneManager.LoadScene(1);
    }
    public void BGMClick()
    {
        if (bgmOn)
        {
            bgmOn = false;
            FindObjectOfType<AudioManager>().Mute("Theme");
        }
        else
        {
            bgmOn = true;
            FindObjectOfType<AudioManager>().UnMute("Theme");
        }
    }
    public void SFXClick()
    {
        if (sfxOn)
        {
            sfxOn = false;
        }
        else
        {
            sfxOn = true;
        }
    }

    public void ButtonClick()
    {
        if(sfxOn)
        {
            FindObjectOfType<AudioManager>().Play("Click");
        }
    }

    public void QuitGame()
    {
        Application.Quit();
        
    }
}
