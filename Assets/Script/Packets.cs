﻿using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Packets
{
    public Sprite[] animal;

    public Sprite[] clothes;

    public Sprite[] objects;

    public Sprite[] people;
}
