using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Randomize : MonoBehaviour
{
    public int check;
    public bool done = false;

    public int maxRandom, childCount;

    // Start is called before the first frame update
    void Start()
    {
        if (SceneManager.GetActiveScene().name == "MatchPicture")
        {
            DoRandomize();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            DoRandomize();
        }

        if(done == true)
        {
            Debug.Log("SELESAI");
        }
        else
        {
            CheckDone();
        }

    }

    public void CheckDone()
    {
        if (transform.GetChild(check).GetComponent<Drag>().on_Pos)
        {
            for (int i = 0; i < maxRandom; i++)
            {
                if (transform.GetChild(i).GetComponent<Drag>().on_Pos)
                {
                    done = true;
                }
                else
                {
                    done = false;
                    Debug.Log("DEBUG LOG RANDOM");
                    if(SceneManager.GetActiveScene().name == "MatchPicture")
                    {
                        DoRandomize();
                    }
                    break;
                }

            }
        }
    }

    public void DoRandomize()
    {
        System.Random ran = new System.Random();
        int index = ran.Next(0, maxRandom);
        for(int i = 0; i <= maxRandom; i++)
        {
            if (transform.GetChild(index).GetComponent<Drag>().on_Pos)
            {
                index = ran.Next(0, maxRandom);
                Debug.Log("MASUK SAMA");
            }
            else
            {
                break;
            }
        }
        Debug.Log(index);
        check = index;
        transform.GetChild(index).gameObject.SetActive(true);
    }
}
