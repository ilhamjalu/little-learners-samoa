﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LearningScript : MonoBehaviour
{
    public ButtonScript bs;
    public LearningManager learningManager;
    Image test;
    GameObject child;
    Button button;

    // Start is called before the first frame update
    void Start()
    {
        child = gameObject.transform.GetChild(0).gameObject;
        test = child.GetComponent<Image>();

        button = gameObject.GetComponent<Button>();
        button.onClick.AddListener(CheckImage);
    }

    // Update is called once per frame
    void Update()
    {
        //
    }

    public void SetImage()
    {
        //bs.image.gameObject.GetComponent<Image>().sprite = test.sprite;
        CheckImage();
    }

    public void CheckImage()
    {
        Debug.Log("TEST");
        for(int i = 0; i < learningManager.image.buttonImage.Length; i++)
        {
            if (test.sprite == learningManager.image.buttonImage[i])
            {
                Debug.Log("DALAM FOR");
                bs.image.gameObject.GetComponent<Image>().sprite = learningManager.image.showImage[i];
            }
            else
            {
                Debug.Log(i);
            }
            
            if(test.sprite == learningManager.image.testSprite[0])
            {
                Debug.Log("berhasil");
            }
        }

        //button.onClick.RemoveAllListeners();
    }
}
