﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using GoogleMobileAds.Api;


public class ButtonScript : MonoBehaviour
{
    public TMP_Text mainImage;
    public Image image;
    public GameObject panelMiniGame, mainButton, playButton;
    public Animator anim, firstAnim, secondAnim, thirdAnim;
    public GameObject animGameObject;


    // Start is called before the first frame update
    void Start()
    {
        //buttonText = gameObject.GetComponentInChildren<TMP_Text>();

        anim = animGameObject.GetComponent<Animator>();
        anim.SetBool("Active", false);
        anim.SetBool("FadeOut", true);

        StartCoroutine(HideObj());

        if(firstAnim != null)
        {
            firstAnim.SetBool("FirstAnim", true);
            secondAnim.SetBool("SecondAnim", true);
            thirdAnim.SetBool("ThirdAnim", true);
        }

        if (PlayerPrefs.GetInt("removeAds") == 0)
        {
            AdsManager.instance.RequestIntestitial();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator HideObj()
    {
        yield return new WaitForSeconds(1);
        animGameObject.SetActive(false);
    }
    
    public void SetImage(TMP_Text a)
    {
        mainImage.SetText(a.text);
        Debug.Log(a.text);
    }

    public void MiniGame()
    {
        panelMiniGame.SetActive(true);
        mainButton.SetActive(false);
    }

    public void BackMenu()
    {
        panelMiniGame.SetActive(false);
        mainButton.SetActive(true);
    }

    public void ChangeScene(string scene)
    {
        animGameObject.SetActive(true);
        anim.SetBool("Active", true);
        StartCoroutine(Transisi(scene));
        
        if(PlayerPrefs.GetInt("removeAds") == 0)
        {
            AdsManager.instance.ShowInterstitial();
        }
    }  

    IEnumerator Transisi(string scene)
    {
        yield return new WaitForSeconds(1.7f);
        SceneManager.LoadScene(scene);
    }

    public void SetGame(string game)
    {
        PlayerPrefs.SetString("game", game);
    }

    public void SetType(string type)
    {
        PlayerPrefs.SetString("type", type);
    }

    public void SetPacket(int packet)
    {
        PlayerPrefs.SetInt("packet", packet);
    }

    public void ShowMainButton()
    {
        playButton.SetActive(false);
        mainButton.SetActive(true);
    }

    public void otherAppsURL(){
        Application.OpenURL("https://play.google.com/store/apps/dev?id=8029258965505671018");
    }

    public void YoutubeURL(){
        Application.OpenURL("https://www.youtube.com/c/Islandize");
    }

    public void WebsURL(){
        Application.OpenURL("https://www.islandize.com.au/");
    }

    public void BGMClick()
    {
        if (Menu.bgmOn)
        {
            Menu.bgmOn = false;
            FindObjectOfType<AudioManager>().Mute("Theme");
        }
        else
        {
            Menu.bgmOn = true;
            FindObjectOfType<AudioManager>().UnMute("Theme");
        }
    }
    public void SFXClick()
    {
        if (Menu.sfxOn)
        {
            Menu.sfxOn = false;
        }
        else
        {
            Menu.sfxOn = true;
        }
    }

    public void ButtonClick()
    {
        if(Menu.sfxOn)
        {
            FindObjectOfType<AudioManager>().Play("Click");
        }
    }

    public void NumberButton(string number){
        if(Menu.sfxOn)
        {
            FindObjectOfType<AudioManager>().Play(number);
        }
    }
}
