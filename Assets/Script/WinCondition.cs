﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinCondition : MonoBehaviour
{
    public GameObject WinText;
    public bool Done = false;
    public int countCheck;

    // Start is called before the first frame update
    void Start()
    {
        WinText.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (!Done)
        {
            Check();
        }
    }

    public void Check()
    {
        for (int i = 0; i < 9; i++)
        {
            if (transform.GetChild(i).GetComponent<Drag>().on_Pos)
            {
                Done = true;
            }
            else
            {
                Done = false;
                i = 9;
            }
        }

        if (Done)
        {
            WinText.SetActive(true);
        }
    }
}
