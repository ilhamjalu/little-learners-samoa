﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PairingManager : MonoBehaviour
{
    public bool countDown;
    public float timeRemaining;
    public TextMeshProUGUI timerText;
    public GameObject gameOver;
    public Randomize randomize;

    public Packets[] packets;

    bool showAds = false;

    public GameObject imageParent;
    public GameObject detector;

    int j;
    
    Sprite[] sprites;

    //public Image test;

    // Start is called before the first frame update
    void Start()
    {
        j = PlayerPrefs.GetInt("packet") - 1;

        ShowDetector();

        if(PlayerPrefs.GetInt("removeAds") == 0)
        {
            AdsManager.instance.RequestIntestitial();
        }
    }

    // Update is called once per frame
    void Update()
    {
        //test.sprite = packets[0].image[0];

        //SetPackets(packets[j].animal);

        // if (countDown)
        // {
        //     if (timeRemaining > 0)
        //     {
        //         timeRemaining -= Time.deltaTime;
        //     }
        //     else
        //     {
        //         timeRemaining = 0;
        //         countDown = false;
        //         GameOver("lose");
        //         Debug.Log("GAME OVER");
        //     }

        //     timerText.text = timeRemaining.ToString("00.00");
        // }

        if (randomize.done == true)
        {
            //Time.timeScale = 0;
            GameOver("win");

            if (PlayerPrefs.GetInt("removeAds") == 0 && showAds == false)
            {
                showAds = true;
                ShowAds(showAds);
            }
        }
    }

    void ShowAds(bool a)
    {
        a = showAds;
        if (a == true)
        {
            AdsManager.instance.ShowInterstitial();
        }
    }

    public void GameOver(string condition)
    {
        gameOver.SetActive(true);
        TextMeshProUGUI text = gameOver.GetComponentInChildren<TextMeshProUGUI>();
        
        if(condition == "lose")
        {
            text.SetText("GAME OVER");
        }
        else
        { 
            text.SetText("WELL DONE");
        }
    }

    public void ShowDetector()
    {
        if(PlayerPrefs.GetString("type") == "animal")
        {
            sprites = packets[j].animal;
        }
        else if(PlayerPrefs.GetString("type") == "clothes")
        {
            sprites = packets[j].clothes;
        }
        else if(PlayerPrefs.GetString("type") == "object")
        {
            sprites = packets[j].objects;
        }
        else
        {
            sprites = packets[j].people;
        }
        
        for(int i = 0; i < sprites.Length; i++)
        {
            Debug.Log(packets.Length);
            detector.transform.GetChild(i).gameObject.SetActive(true);
        }

        SetPackets(sprites);
        randomize.maxRandom = sprites.Length;  
    }

    public void SetPackets(Sprite[] p)
    {
        for(int i = 0; i < p.Length; i++)
        {
            Debug.Log("TEST");

            if(PlayerPrefs.GetString("type") == "animal")
            {
                imageParent.transform.GetChild(i).gameObject.GetComponent<Image>().sprite = packets[j].animal[i];
                detector.transform.GetChild(i).gameObject.GetComponent<Image>().sprite = packets[j].animal[i];
            }
            else if(PlayerPrefs.GetString("type") == "clothes")
            {
                imageParent.transform.GetChild(i).gameObject.GetComponent<Image>().sprite = packets[j].clothes[i];
                detector.transform.GetChild(i).gameObject.GetComponent<Image>().sprite = packets[j].clothes[i];
            }
            else if(PlayerPrefs.GetString("type") == "object")
            {
                imageParent.transform.GetChild(i).gameObject.GetComponent<Image>().sprite = packets[j].objects[i];
                detector.transform.GetChild(i).gameObject.GetComponent<Image>().sprite = packets[j].objects[i];
            }
            else
            {
                imageParent.transform.GetChild(i).gameObject.GetComponent<Image>().sprite = packets[j].people[i];
                detector.transform.GetChild(i).gameObject.GetComponent<Image>().sprite = packets[j].people[i];
            }
        }
    }
}
