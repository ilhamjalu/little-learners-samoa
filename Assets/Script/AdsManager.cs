﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using UnityEngine.SceneManagement;
using System; 

public class AdsManager : MonoBehaviour
{
    private BannerView bannerAd;
    InterstitialAd interstitialAd;
    public string bannerId, interstitial;

    public static AdsManager instance;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Banner();
    }

    // Update is called once per frame
    void Update()
    {
        if(PlayerPrefs.GetInt("removeAds") == 1)
        {
            Debug.Log("REMOVE ADS");
            bannerAd.Destroy();
        }
    }

    private AdRequest CreateAdRequest()
    {
        return new AdRequest.Builder().Build();
    }

    public void Banner()
    {
        if (PlayerPrefs.GetInt("removeAds") == 0)
        {
            MobileAds.Initialize(InitializationStatus => { });
            RequestBanner();
            Debug.Log("SUKSES");
        }
    }

    public void RequestBanner()
    {
        bannerAd = new BannerView(bannerId, AdSize.SmartBanner, AdPosition.Bottom);
        bannerAd.LoadAd(CreateAdRequest());
    }

    public void RequestIntestitial()
    {
        if(interstitialAd != null)
        {
            interstitialAd.Destroy();
        }

        interstitialAd = new InterstitialAd(interstitial);
        interstitialAd.LoadAd(CreateAdRequest());
    }

    public void ShowInterstitial()
    {
        if (interstitialAd.IsLoaded())
        {
            interstitialAd.Show();
        }
        else
        {
            Debug.Log("Interstitial Not Loaded");
        }
    }
}
