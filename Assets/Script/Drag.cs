﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Drag : MonoBehaviour
{
    public GameObject detector;
    Vector3 first_Pos, first_Scale;
    public bool on_Pos = false;

    // Start is called before the first frame update
    void Start()
    {
        first_Pos = transform.position;
        first_Scale = transform.localScale;
    }

    private void OnMouseDrag()
    {
        Vector3 pos_Mouse = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));
        transform.position = new Vector3(pos_Mouse.x, pos_Mouse.y, transform.position.z);
        transform.localScale = new Vector2(0.4f, 0.4f);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject == detector)
        {
            on_Pos = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject == detector)
        {
            on_Pos = false;
        }
    }
 
    private void OnMouseUp()
    {
        if (on_Pos)
        {
            transform.position = detector.transform.position;

            Scene scene = SceneManager.GetActiveScene();

            if(scene.name == "MatchPicture")
            {
                transform.localScale = first_Scale;
            }
            else
            {
                transform.localScale = new Vector2(0.4f, 0.4f);
            }
        }
        else
        {
            transform.position = first_Pos;
            transform.localScale = first_Scale;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
