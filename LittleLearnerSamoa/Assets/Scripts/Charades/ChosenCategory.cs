﻿using UnityEngine;
[System.Serializable]
public class ChosenCategory
{
    public Sprite image;
    public string englishLang;
    public string samoanLang;
}
