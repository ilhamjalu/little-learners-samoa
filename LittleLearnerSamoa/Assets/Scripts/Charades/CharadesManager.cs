﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
public class CharadesManager : MonoBehaviour
{
    public float timeRemaining;
    public bool countDown = true;
    private int score;

    [Header("Panel UI")]
    [SerializeField] private TextMeshProUGUI timerText;
    [SerializeField] private GameObject backPanel;
    [SerializeField] private GameObject gameOverPanel;
    [SerializeField] private GameObject optionPanel;
    [SerializeField] private TextMeshProUGUI scoreText, gameOverText;


    [Header("Content Category")]
    [SerializeField] private GameObject imageContainer;
    [SerializeField] private TextMeshProUGUI englishText;
    [SerializeField] private TextMeshProUGUI samoanText;

    [Header("Category Content")]
    public ChosenCategory[] categoryContents;


    void Start()
    {
        SetContent(RandomIndexContent());
        countDown = true;
    }


    void Update()
    {
        if (countDown)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
            }
            else
            {
                timeRemaining = 0;
                countDown = false;
                GameOver();
            }
            
            timerText.text = timeRemaining.ToString("00.00");
        }
      
    }

    private void SetContent(int r)
    {
        imageContainer.GetComponent<Image>().sprite = categoryContents[r].image;
        englishText.GetComponent<TextMeshProUGUI>().text = categoryContents[r].englishLang;
        samoanText.GetComponent<TextMeshProUGUI>().text = categoryContents[r].samoanLang;
    }
    private int RandomIndexContent()
    {
        int x = Random.Range(0, categoryContents.Length);
        print(x);
        return x;
    }

    public void NextContent()
    {
        SetContent(RandomIndexContent());
    }

    public void BackButton()
    {
        SceneManager.LoadScene("CH_CategorySelect");
    }


    public void ActivePanel()
    {
        if (optionPanel.activeInHierarchy || backPanel.activeInHierarchy)
        {
            countDown = false;
        }
        else
        {
            countDown = true;
        }
    }

    public void GameOver()
    {
        gameOverText.text = "Game Over \n \n Your Score is " + score.ToString();
        gameOverPanel.SetActive(true);
    }

    public void CorrectButton()
    {
        score += 1;
        scoreText.text = score.ToString();
    }

}
